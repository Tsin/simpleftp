#include <stdio.h>
#include <iostream>
#include <fstream>
#include <WinSock2.h> //��� ����� ��� �������� ������� ��� ������ � ��������
#include <regex> //����� ��� ������������� IP-������ � ������� ���������� ��������� 
#include "commands.h" //������ ������

WSADATA wsaData; //�������� ��������� ��� ������������� �������
SOCKET cmdSocket, dataSocket; //������ ��� �������� ������ � ������ 
char addr[256]; //���������� ��� �������� ������ �������
char fd_req[128], fd_name[128]; //���������� ��� ��������� �� ������ � ������
int cmd; //���������� ��� �������� ������� �� ������������

//���������� �������
int init_wsa();
int init_connection();
int init_data();
int resolve_addr();
int resolve_cmd();
int send_msg(SOCKET sock, char* msg, bool show);
int receive_msg(SOCKET sock, bool show);
int receive_data_msg(SOCKET dsock, bool save, char* file_name);

int main()
{
	init_wsa(); //�������������� wsaData
	
	//��������� ����� �������
	if (resolve_addr() == -1)
		return 0;

	init_connection(); //������������� ���������� � ��������

	while (true)
	{
		cmd = resolve_cmd(); //��������� ������� �� ������������

		switch (cmd)
		{
		case CMD_LOGIN: // ����������� �� �������
			send_msg(cmdSocket, "USER anonymous", true);
			receive_msg(cmdSocket, true);
			send_msg(cmdSocket, "PASS anonymous@mail.ru", true);
			receive_msg(cmdSocket, true);
			break; 
		case CMD_LIST: //��������� ������ ������
			init_data();
			
			send_msg(cmdSocket, "LIST", true);
			receive_msg(cmdSocket, true);
			receive_data_msg(dataSocket, false, NULL);
			
			closesocket(dataSocket);
			receive_msg(cmdSocket, true);
			break; 
		case CMD_PWD: //����� ������� ���������� �� �����
			send_msg(cmdSocket, "PWD", false);
			receive_msg(cmdSocket, true);
			break;
		case CMD_CWD: //����� ����������

			//��������� ��� ���������� ��� ���������
			printf("Enter directory name: ");
			scanf("%s", &fd_name);
			sprintf(fd_req, "CWD %s", fd_name);

			send_msg(cmdSocket, fd_req, false);
			receive_msg(cmdSocket, true);
			break;
		case CMD_GET: //���������� �����
			init_data();

			//��������� ��� ����� ��� ����������
			printf("Enter file name: ");
			scanf("%s", &fd_name);
			sprintf(fd_req, "RETR %s", fd_name);

			send_msg(cmdSocket, fd_req, false);
			receive_msg(cmdSocket, true);
			receive_data_msg(dataSocket, true, fd_name);
			
			closesocket(dataSocket);
			receive_msg(cmdSocket, true);
			break;
		case CMD_QUIT:
			send_msg(cmdSocket, "QUIT", true);
			closesocket(cmdSocket);
			closesocket(dataSocket);
			WSACleanup();
			system("pause");
			return 0;
			break;
		case CMD_HELP:
			printf("Commands:");
			printf("\nlogin - authentification as Anonymous");
			printf("\nquit - exit server");
			printf("\nlist - get list of files and directories");
			printf("\nget - retrieve file from server");
			printf("\ndir - show curent directory");
			printf("\ncd - change directory");
			printf("\nhelp - show this message\n");
			break;
		default:
			printf("Unknown command. Try again.\n");
			break;
		}
	}

	return 0;
}

//������������� ��������� wsaData ��� ������ �������
int init_wsa()
{
    int result;

    result = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (result != NO_ERROR) 
	{
        printf("WSAStartup failed with error: %d\n", result);
        return 1;
    }

	return 0;
}

//��������� ���������� � ��������
int init_connection()
{
	int res;
	sockaddr_in address; 

	//�������������� �����
	cmdSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (cmdSocket == INVALID_SOCKET) 
	{
        printf("socket failed with error: %ld\n", WSAGetLastError());
        WSACleanup();
        return 1;
    }

	//��������� ��������� address
    address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr(addr);
	address.sin_port = htons(PORT_COMMAND);

	//������������� ����������� � ������ �� ������� �������
	res = connect(cmdSocket, (SOCKADDR*)&address, sizeof(address));
    if (res == SOCKET_ERROR) 
	{
        printf("Connection failed with error: %d\n", WSAGetLastError());
		closesocket(cmdSocket);
        WSACleanup();
        return 1;
	}

	//�����, ��� ���������� ������� ����������� � ��������� ������ ����� � �������
	printf("Connected to %s\n", addr);
	receive_msg(cmdSocket, true);

	return 0;
}

//�������������� ����� ��� �������� ������
int init_data()
{
	//����������� � ������� ������� � ��������� �����
	send(cmdSocket, "PASV\r\n", strlen("PASV\r\n"), 0);
	
	//������� ����� � ���������� ������
	char buff[128] = {' '};
	recv(cmdSocket, buff, 128, 0);

	//����� �������� � ����: 227 Entering Passive Mode (192,168,254,253,207,56)
	//� tmp_char ������� ������ "192,168,254,253,207,56"
	char *tmp_char;
	tmp_char = strtok(buff, "("); //������� ������ ������ ('
	tmp_char = strtok(NULL, "("); //�������� ����������� ������ � ������� '('
	tmp_char = strtok(tmp_char, ")"); //����������� ������������, ����� ������ ')'
	
	int a, b; //����� ��� ����������� ����� ������
	int c, d, e, f; //����� ������� ������������ �� �����
	sscanf(tmp_char, "%d,%d,%d,%d,%d,%d", &c, &d, &e, &f, &a, &b);

	int len, result, port = a * 256 + b;
    
	//�������������� ����� ������
	dataSocket = socket(AF_INET, SOCK_STREAM, 0);
    
	sockaddr_in address;
	address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr(addr);
    address.sin_port = htons(port); //������������� ���� �� ��������, ������� ������� ������
    
	len = sizeof(address);

	//����������� � ������� ������
	result = connect(dataSocket, (sockaddr*)&address, len);
    if (result == -1)
	{
		printf("Data socket connection failure.");
		return -1;
	}

	printf("Connected to data socket on port %d\n",  port);

	return 0;
}

//��������� ����� ������� � ����������, ���������� ��� ��� (ip-���� ��� url) � ����������� � ���� IP-������
int resolve_addr()
{
	printf("Enter host address or IP: ");
	scanf("%s", &addr);

	//if probably NOT ip
	if (!std::regex_match(addr, std::regex(IP_PATTERN)))
	{
		hostent *h;
		sockaddr_in address;

		h = gethostbyname(addr);
		if (!h)
		{
			printf("Could not resolve host name.\n");
			system("pause");
			return -1;
		}
		sprintf(addr, "%s", inet_ntoa(*(in_addr*)h->h_addr_list[0]));
	}
	return 0;
}

//���������� ������� �� ������������
int resolve_cmd()
{
	char cmd[64] = {' '};
	printf(">>");
	scanf("%s", cmd);
	
	if (strcmp(cmd, CMD_LOGIN_STR) == 0)		return CMD_LOGIN;
	else if (strcmp(cmd, CMD_QUIT_STR) == 0)	return CMD_QUIT;
	else if (strcmp(cmd, CMD_LIST_STR) == 0)	return CMD_LIST;
	else if (strcmp(cmd, CMD_PWD_STR) == 0)		return CMD_PWD;
	else if (strcmp(cmd, CMD_CWD_STR) == 0)		return CMD_CWD;
	else if (strcmp(cmd, CMD_GET_STR) == 0)		return CMD_GET;
	else if (strcmp(cmd, CMD_HELP_STR) == 0)		return CMD_HELP;

	return CMD_ERROR;
}

//�������� ������� �� ������, show ���������� ����� �� ���������� ����� ������� �� ������
int send_msg(SOCKET sock, char* msg, bool show)
{
	char cmd[128] = {' '};
	sprintf(cmd, "%s\r\n", msg); //��������� ������� ��� ������� ����� ���������� � ����� \r\n
	send(sock, cmd, strlen(cmd), 0); //���������� ������� �� ������

	//������� ������� �� �����
	if (show)
		printf("Request: %s", cmd); 

	return 0;
}

//��������� ����� �� �������. show ���������� ����� �� ���������� ����� ������� �� ������
int receive_msg(SOCKET sock, bool show)
{
	int read; //������ ���������� ����������, �������� � �������
    fd_set fdr;
    FD_ZERO(&fdr);
    FD_SET(sock, &fdr);
 
	//����� ���������� ������ � �������� � 1 �������
	timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;  

	char buffer[512] = {' '};

	if (show)
		printf("Response: ");

	do 
	{
        recv(sock, buffer, 512, 0); //�������� ����� � �������
        
		//������� ����� �� �����, ���� ���������
		if (show)
			printf("%s", buffer); 
        
		read = select(sock + 1, &fdr, NULL, NULL, &timeout); //���������, ���� �� �� ������ ��� ������ ��� ��������
    } while(read);
	
	return 0;
}

//��������� ������ �� �������, save ���������� ����� �� �������� ����� ������� � ���� 
int receive_data_msg(SOCKET dsock, bool save, char* file_name)
{
	int rc;
	char buffer[4096] = {' '};

	if (!save) // ���� ����� ��������� �� �����, �� ������� ��� �� �����
	{
		printf("Response:\n");
		do
		{
			//�������� ������, �, ���� ����� �� ����, �� ������� ��� �� �����
			rc = recv(dsock, buffer, 4096, 0); 
			if (rc != 0)
				printf("%s", buffer);
		} while(rc);
	}
	else //��������� ����� � ����
	{
		std::ofstream file(file_name, std::ios::binary); 
		do
		{
			rc = recv(dsock, buffer, 4096, 0); 
			if (rc != 0)
				file << buffer;
		} while(rc);

		file.close();
		printf("File saved.\n");
	}

	return 0;
}